flag = True
total = 0


def validate(number):
    number = number.replace(" ", "")
    if number.isdigit():
        return int(number)
    else:
        return None


while flag:
    number_user = input()
    number = validate(number_user)
    if number:
        digits = [int(i) for i in str(number)]
        total += sum(digits)
    else:
        flag = False

if total:
    print(total)
