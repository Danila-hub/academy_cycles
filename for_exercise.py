# Задание на for
content = input("Введите слова, разделённые любым символом: ").lower()
if content[-1].isalpha():
    content += "."

words_list = []
temp_word = ""

for letter in content:
    if letter.isalpha() or letter in "-`":
        temp_word += letter
    elif len(temp_word) > 3:
        words_list.append(temp_word)
        temp_word = ""
    else:
        temp_word = ""
print(", ".join(words_list))
